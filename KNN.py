import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier

dataset = pd.read_csv('titanic_all.csv')

# print(dataset)
# print(dataset.columns.values)

dataset = dataset.drop(["PassengerId", "Name", "Sex", "SibSp", "Parch", "Ticket", "Fare", "Cabin", "Embarked"], axis=1)
dataset = dataset.dropna()

print(dataset)

dataset = dataset.to_numpy()

# print(dataset)

data_training = np.concatenate((dataset[0:261, :], dataset[348:609, :], dataset[696:957, :]), axis=0)
data_testing = np.concatenate((dataset[261:348, :], dataset[609:696, :], dataset[957:1046, :]), axis=0)

# print("Data training : ", data_training)
# print("Data testing : ", data_testing)

input_training = data_training[:, 1:3]
label_training = data_training[:, 0]
input_testing = data_testing[:, 1:3]
label_testing = data_testing[:, 0]

# print("Input training : ", input_training)
# print("Label training : ", label_training)

# Mendefinisikan metode KNN
KNN = KNeighborsClassifier(n_neighbors=3, weights='distance')

# Training
KNN = KNN.fit(input_training, label_training)

# Prediksi
hasil = KNN.predict(input_testing)

print("Target sebenernya ", label_testing)
print("Hasil prediksi : ", hasil)

prediksi_benar = (hasil == label_testing).sum()
prediksi_salah = (hasil != label_testing).sum()

# Akurasi
print("Prediksi benar : ", prediksi_benar)
print("Prediksi salah : ", prediksi_salah)
print("Akurasi : ", prediksi_benar/(prediksi_benar+prediksi_salah)*100, "%")
